# Obfuscate

**This add-on change the font on a page to use [Flow Circular](https://danross.co/flow/).**

Useful to take a screenshot of a page to show a design without showing the actual text.

## How to use

### On Firefox

Download and install it from here : [https://addons.mozilla.org/firefox/addon/obfuscate/](https://addons.mozilla.org/firefox/addon/obfuscate/)

On any page, use the context menu (usually with a right-click) to obfuscate a page.
You can't remove the obfuscation without refreshing the page.

Note that it won't works on addons.mozilla.org as it's a protected domain.

### On Chromium-based browsers

On any page, click on the extension icon to obfuscate a page.
You can't remove the obfuscation without refreshing the page.

## Credits
This extension is licensed under the GNU GPLv3.
Orignal idea by [@maris@chaos.social](https://chaos.social/@maris): https://chaos.social/@maris/108379392423588799

The Flow Circular is made by Dan Ross under the SIL Open Font License.
