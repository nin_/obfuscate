const CSS = `@font-face{font-family:'FC';src:url('<url>');}*{font-family:'FC'!important;}`
.replace('<url>', chrome.runtime.getURL("FlowCircular.ttf"))

chrome.action.onClicked.addListener((tab) => {
  chrome.scripting.insertCSS({ target: {tabId: tab.id}, css: CSS })
});
